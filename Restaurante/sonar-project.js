const sonarqubeScanner = require('sonarqube-scanner');
sonarqubeScanner({
    serverUrl: 'http://localhost:9000',
    options: {
        'sonar.sources': 'Restaurante',
        'sonar.tests': 'Restaurante',
        'sonar.inclusions': '**',
        'sonar.test.inclusions': 'Restaurante/**/*.spec.js,Restaurante/**/*.spec.jsx,Restaurante/**/*.test.js,Restaurante/**/*.test.jsx',
        'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info',
        'sonar.testExecutionReportPaths': 'coverage/test-reporter.xml'
    }
}, () => {});