const request = require('supertest');
const server = require('../index');

describe('Get Endpoints', () => {
    it('Get', async(done) => {
        const res = await request(server)
            .get('/pedido/')
            .send({
                id: 1,
            });
        expect(res.statusCode).toEqual(200).rejects.toThrow(Error('Something failed'));
        done();
    })
})
afterAll(async done => {
    server.close();
    done();
});